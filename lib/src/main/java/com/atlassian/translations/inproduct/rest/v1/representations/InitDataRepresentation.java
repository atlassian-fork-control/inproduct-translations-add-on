package com.atlassian.translations.inproduct.rest.v1.representations;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@XmlRootElement
public class InitDataRepresentation {
	@XmlElement private boolean allowed;
	@XmlElement private boolean isAnyTranslation;
    @XmlElement private boolean isAdmin;
    @XmlElement private String product;

	public InitDataRepresentation() {
	}

	public InitDataRepresentation(boolean allowed, boolean isAnyTranslation, boolean isAdmin, String product) {
		this.allowed = allowed;
		this.isAnyTranslation = isAnyTranslation;
        this.isAdmin = isAdmin;
        this.product = product;
	}
}
