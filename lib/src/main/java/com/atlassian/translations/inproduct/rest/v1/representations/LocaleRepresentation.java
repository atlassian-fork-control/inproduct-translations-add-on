package com.atlassian.translations.inproduct.rest.v1.representations;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Locale;

/**
 * User: kalamon
 * Date: 03.07.12
 * Time: 10:41
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LocaleRepresentation {
    @XmlElement private String name;
    @XmlElement private String displayName;

    public LocaleRepresentation(Locale locale, Locale displayIn) {
        this.displayName = locale.getDisplayName(displayIn != null ? displayIn : locale);
        this.name = locale.toString();
    }

    public LocaleRepresentation() {
    }
}
