package com.atlassian.translations.inproduct.components;

import java.io.File;
import java.io.IOException;

/**
 * User: kalamon
 * Date: 20.06.13
 * Time: 15:20
 */
public interface LanguagePackGenerator {
    File generate(String language) throws IOException;
}
