package com.atlassian.translations.inproduct.rest.v1.representations;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * User: kalamon
 * Date: 03.07.12
 * Time: 10:41
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LocalesRepresentation {
    @XmlElement private LocaleRepresentation currentLocale;
    @XmlElement private List<LocaleRepresentation> supportedLocales = new ArrayList<LocaleRepresentation>();

    public LocalesRepresentation(Locale currentLocale, List<Locale> supportedLocales) {
        this.currentLocale = new LocaleRepresentation(currentLocale, null);
//        for (Locale locale : supportedLocales) {
//            this.supportedLocales.add(new LocaleRepresentation(locale, currentLocale));
//        }
    }

    public LocalesRepresentation() {
    }
}
