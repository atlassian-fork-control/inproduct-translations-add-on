(function ($) {
    var settings = {
        style: '',
        text: '',
        title: 'Find',
        changed: function(text) {}
    };

    var text = '';

    var methods = {
        init: function(options) {
            settings = $.extend(settings, options);
            return this.each(function(i, o) {
                $(o).attr('title', settings.title);
                $(o).parent().prepend('<div class="translated-message-searcher-mag"></div>');
                $(o).parent().append('<div class="translated-message-searcher-close"></div>');
                $(o).focus(function() {
                    if ($(this).val() == $(this)[0].title) {
                        $(this).removeClass('translated-message-searcher-initial');
                        $(this).val("");
                        $(o).parent().find('.translated-message-searcher-close').show();
                    }
                });

                $(o).blur(function() {
                    if ($(this).val() == '') {
                        $(this).addClass('translated-message-searcher-initial');
                        $(this).val($(this)[0].title);
                        text = '';
                        $(o).parent().find('.translated-message-searcher-close').hide();
                    }
                });

                $(o).keyup(function() {
                    text = $(o).val();
                    settings.changed(text);
                });

                $(o).parent().find('.translated-message-searcher-close').click(function() {
                    $(o).val('');
                    text = '';
                    $(o).blur();
                    settings.changed('');
                });

                $(o).parent().find('.translated-message-searcher-close').mouseover(function() {
                    $(this).addClass('hover');
                });
                $(o).parent().find('.translated-message-searcher-close').mouseout(function() {
                    $(this).removeClass('hover');
                });

                $(o).blur();
            });
        },

        getText: function() {
            return text;
        }
    };

    $.fn.searcher = function(method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || ! method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Method ' +  method + ' does not exist on jQuery.searcher');
        }
    };
})(jQuery);
