package com.atlassian.translations.inproduct.components;

import com.atlassian.translations.inproduct.JiraIptUtils;

import java.util.Locale;

/**
 *
 *
 * @since v5.0
 */
public class PreviousLocaleManager {

	Locale previousLocale;

	/**
	 * @return true if locale has changed since last call
	 */
	public boolean hasLocaleChanged() {

		Locale locale = JiraIptUtils.getI18nHelper().getLocale();

		// cannot determine locale
		if (locale == null) {
			return false;
		}

		// the same locale
		if (previousLocale != null && previousLocale.equals(locale)) {
			return false;
		}

		previousLocale = locale;

		return true;
	}
}
