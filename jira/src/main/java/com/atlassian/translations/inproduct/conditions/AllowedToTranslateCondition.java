package com.atlassian.translations.inproduct.conditions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractJiraCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.translations.inproduct.components.JiraAccessManager;

/**
 * @author jjaroczynski
 *
 * @since v5.0
 */
public class AllowedToTranslateCondition extends AbstractJiraCondition {

	private JiraAccessManager accessManager;

	public AllowedToTranslateCondition(JiraAccessManager accessManager) {
		this.accessManager = accessManager;
	}

	public boolean shouldDisplay(User user, JiraHelper jiraHelper) {
		if (user != null) {
			return shouldDisplay(user.getName());
		}

		return false;
	}

	public boolean shouldDisplay(ApplicationUser applicationUser, JiraHelper jiraHelper) {
		if (applicationUser != null) {
			return shouldDisplay(applicationUser.getName());
		}

		return false;
	}

	private boolean shouldDisplay(String userName) {
		return accessManager.isUserAllowedToTranslate(userName);
	}
}
