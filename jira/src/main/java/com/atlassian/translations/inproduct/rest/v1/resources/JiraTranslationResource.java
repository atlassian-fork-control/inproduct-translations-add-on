package com.atlassian.translations.inproduct.rest.v1.resources;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.PluginResourceLocator;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.translations.inproduct.JiraIptUtils;
import com.atlassian.translations.inproduct.JiraMessageTransformUtil;
import com.atlassian.translations.inproduct.components.AccessManager;
import com.atlassian.translations.inproduct.components.MessageKeyGuesser;
import com.atlassian.translations.inproduct.components.TacIntegrationManager;
import com.atlassian.translations.inproduct.components.TranslationManager;
import com.google.common.collect.ImmutableMap;
import org.apache.log4j.Logger;
import com.google.common.collect.Sets;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;
import java.util.Set;
import java.util.Map;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * User: kalamon, jjaroczynski
 * Date: 29.06.12
 */
@Path("translate")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@AnonymousAllowed
public class JiraTranslationResource extends AbstractTranslationResource {
    private final PermissionManager permissionManager;
    private final PluginResourceLocator pluginResourceLocator;
    private final EventPublisher eventPublisher;
    private final PluginAccessor pluginAccessor;

	private static final Logger LOG = Logger.getLogger(JiraTranslationResource.class);
    private static final Map<String, String> PLUGIN_KEY_TO_TAC_NAME_MAPPING = ImmutableMap.<String, String>builder()
            .put("com.atlassian.jira.jira-software-application", "JIRA Software")
            .put("com.atlassian.servicedesk.application", "JIRA Service Desk")
            .build();

	public JiraTranslationResource(
            TranslationManager translationManager, MessageKeyGuesser guesser,
            AccessManager accessManager, PermissionManager permissionManager,
            PluginResourceLocator pluginResourceLocator, TacIntegrationManager tacIntegrationManager,
            EventPublisher eventPublisher, PluginAccessor pluginAccessor) {

        super(translationManager, guesser, accessManager, tacIntegrationManager);

        this.permissionManager = permissionManager;
        this.pluginResourceLocator = pluginResourceLocator;
        this.eventPublisher = eventPublisher;
        this.pluginAccessor = pluginAccessor;
	}

    @Override
    protected void clearWebCache() {
        JiraMessageTransformUtil.clearWebResourceCache(pluginResourceLocator, eventPublisher, pluginAccessor);
    }

    @Override
    protected boolean isCurrentUserAdmin() {
        return permissionManager.hasPermission(Permissions.ADMINISTER, JiraIptUtils.getCurrentUser());
    }

    @Override
    protected Locale getCurrentLocale() {
        return JiraIptUtils.getI18nHelper().getLocale();
    }

    @Override
    protected String getUnescapedText(String key) {
        return JiraIptUtils.getI18nHelper().getUnescapedText(key);
    }

    @Override
    protected String getUntransformedRawText(String key) {
        return JiraIptUtils.getI18nHelper().getUntransformedRawText(key);
    }

    @Override
    protected String getProduct() {
        return "JIRA";
    }

    @Override
    protected String getProductVersion() {
        BuildUtilsInfo bui = JiraIptUtils.getComponentInstanceOfType(BuildUtilsInfo.class);
        return bui.getVersion();
    }

    @Override
    protected Map<String, String> getProductsAndVersions() {

        Map<String, String> versions = PLUGIN_KEY_TO_TAC_NAME_MAPPING.keySet().stream()
                .map(pluginAccessor::getPlugin)
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(
                        plugin -> PLUGIN_KEY_TO_TAC_NAME_MAPPING.get(plugin.getKey()),
                        plugin -> plugin.getPluginInformation().getVersion()
                        )
                );

        versions.put("JIRA Core", getProductVersion());
        return versions;
    }

    @GET
	@Path("initdata")
	@Consumes({MediaType.WILDCARD})
	public Response getInitData() {
        return super.getInitData();
	}

	@GET
	@Path("strings")
	@Consumes({MediaType.WILDCARD})
	public Response getStrings() {
        return super.getStrings();
	}

	@GET
	@Path("locales")
	@Consumes({MediaType.WILDCARD})
	public Response getLocales() {
        return super.getLocales();
	}

	@GET
	@Path("original_message/{key}/get")
	@Consumes({MediaType.WILDCARD})
	public Response getOriginalMessage(@PathParam("key") final String messageKey) {
        return super.getOriginalMessage(messageKey);
	}

    @POST
    @Path("toggle")
    public Response toggleTranslationMode() {
        return super.toggleTranslationMode();
    }

	@POST
	@Path("guesskeys")
	@Consumes({MediaType.WILDCARD})
	public Response guessKeys(@FormParam("keysandmessages") String keysAndMessagesJson) {
        return super.guessKeys(keysAndMessagesJson);
	}


	@POST
	@Path("original_message/validate")
	@Consumes({MediaType.WILDCARD})
	public Response validateTranslation(
            @FormParam("original") String original,
            @FormParam("translation") String translation) {

        return super.validateTranslation(original, translation);
	}

	@POST
	@Path("original_message/{key}/save")
	@Consumes({MediaType.WILDCARD})
	public Response saveTranslation(
            @PathParam("key") final String messageKey,
			@FormParam("original") final String original,
			@FormParam("translation") final String translation,
			@FormParam("locale") final String locale) {

        return super.saveTranslation(messageKey, original, translation, locale);
	}

	@POST
	@Path("original_message/{key}/delete")
	@Consumes({MediaType.WILDCARD})
	public Response deleteTranslation(
            @PathParam("key") final String messageKey,
            @FormParam("locale") final String locale) {
        return super.deleteTranslation(messageKey, locale);
	}

	@POST
	@Path("original_message/delete")
	@Consumes({MediaType.WILDCARD})
	public Response deleteAllTranslations(@FormParam("locale") final String locale) {
        return super.deleteAllTranslations(locale);
	}

    @POST
    @Path("uploadtotac")
    @Consumes({MediaType.WILDCARD})
    public Response uploadToTac(
            @FormParam("locale") final String locale,
            @FormParam("login") final String login,
            @FormParam("password") final String password) {
        return super.uploadToTac(locale, login, password);
    }

    @POST
    @Path("contributetotac")
    @Consumes({MediaType.WILDCARD})
    public Response contributeToTac(
            @FormParam("locale") final String locale,
            @FormParam("login") final String login,
            @FormParam("password") final String password) {
        return super.contributeToTac(locale, login, password);
    }
}
